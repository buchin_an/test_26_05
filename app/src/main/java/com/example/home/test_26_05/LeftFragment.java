package com.example.home.test_26_05;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class LeftFragment extends Fragment implements InteractListener {

    public static LeftFragment getInstance(){
        return new LeftFragment();
    }

    TextView label;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_right, container, false);
        label = root.findViewById(R.id.label);
        return root;
    }

    @Override
    public void show(String text) {
        label.setText(text);
    }
}
