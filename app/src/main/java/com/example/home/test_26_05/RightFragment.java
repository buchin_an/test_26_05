package com.example.home.test_26_05;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

public class RightFragment extends Fragment {

    public static RightFragment getInstanse(){
        return new RightFragment();
    }

    EditText editText;
    InteractListener interactListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_left, container, false);
        editText = root.findViewById(R.id.input);
        root.findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (interactListener != null) {
                    interactListener.show(editText.getText().toString());
                }
            }
        });
        return root;
    }

    public void setInteractListener(InteractListener interactListener) {
        this.interactListener = interactListener;
    }


}
