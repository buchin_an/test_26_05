package com.example.home.test_26_05;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LeftFragment leftFragment = LeftFragment.getInstance();
        RightFragment rightFragment = RightFragment.getInstanse();
        rightFragment.setInteractListener(leftFragment);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container_left, leftFragment)
                .commit();

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container_right, rightFragment)
                .commit();

    }


}
